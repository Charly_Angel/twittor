# twittor

sitio web basado en twitter construido con react + vite

## `getting started`

```bash
# install dependencies
yarn install
```

## `development`

```bash
# run app
yarn dev
```

## `build`

```bash
yarn build
```

## `preview`

```bash
yarn preview
```
