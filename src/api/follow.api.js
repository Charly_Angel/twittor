import axios from "axios";
import endpoints from "api/endpoints.api";

export async function checkFollow({ id }) {
  try {
    const { data } = await axios.get(endpoints.follow.check({ id }));
    return { error: false, data, message: "" };
  } catch (error) {
    return {
      error: true,
      data: error.response.data,
      message: error.response.data.message,
    };
  }
}

export async function follow({ id }) {
  try {
    const { data } = await axios.post(endpoints.follow.new, {
      userId: id,
    });
    return { error: false, data, message: "" };
  } catch (error) {
    return {
      error: true,
      data: error.response.data,
      message: error.response.data.message,
    };
  }
}

export async function unFollow({ id }) {
  try {
    const { data } = await axios.delete(endpoints.follow.delete({ id }));
    return { error: false, data, message: "" };
  } catch (error) {
    return {
      error: true,
      data: error.response.data,
      message: error.response.data.message,
    };
  }
}
