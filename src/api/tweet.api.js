import axios from "axios";
import endpoints from "api/endpoints.api";

export async function tweetApi(message) {
  try {
    const { data } = await axios.post(endpoints.tweet.new, { message });
    return { error: false, data, message: "Tweet enviado" };
  } catch (error) {
    return { error: true, data: null, message: "Error al enviar el tweet" };
  }
}

export async function getTweetsApi({ endpoint }) {
  try {
    const { data } = await axios.get(endpoint);
    return { error: false, data, message: "" };
  } catch (error) {
    return {
      error: true,
      data: null,
      message: "Error al recuperar los tweets",
    };
  }
}
