import axios from "axios";
import endpoints from "api/endpoints.api";

export async function signUp(user) {
  try {
    const { data } = await axios.post(endpoints.auth.register, user);
    return { error: false, data, message: "El registro ha sido exitoso" };
  } catch (error) {
    return {
      error: true,
      data: error.response.data,
      message: error.response.data.message,
    };
  }
}

export async function signIn({ email, password }) {
  try {
    const { data } = await axios.post(endpoints.auth.login, {
      email,
      password,
    });
    return { error: false, data, message: "" };
  } catch (error) {
    return {
      error: true,
      data: error.response.data,
      message: error.response.data.message,
    };
  }
}
