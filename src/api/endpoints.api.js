export const BASE_URL = import.meta.env.VITE_BASE_URL;

const endpoints = {
  auth: {
    register: `${BASE_URL}/api/auth/register`,
    login: `${BASE_URL}/api/auth/login`,
  },
  follow: {
    check: ({ id }) => `${BASE_URL}/api/follower/${id}/check`,
    new: `${BASE_URL}/api/follower`,
    delete: ({ id }) => `${BASE_URL}/api/follower/${id}`,
  },
  tweet: {
    new: `${BASE_URL}/api/tweet`,
  },
  tweets: {
    user: ({ id }) => `${BASE_URL}/api/profile/${id}/tweets`,
    home: `${BASE_URL}/api/profile/tweets/followings`,
  },
  profile: {
    get: ({ id }) => `${BASE_URL}/api/profile/${id}`,
    uploadBanner: `${BASE_URL}/api/profile/upload-banner`,
    uploadAvatar: `${BASE_URL}/api/profile/upload-avatar`,
    update: `${BASE_URL}/api/profile`,
    followings: ({ id, page }) =>
      `${BASE_URL}/api/profile/${id}/followings?page=${page}`,
    followers: ({ id, page }) =>
      `${BASE_URL}/api/profile/${id}/followers?page=${page}`,
    search: ({ query }) => `${BASE_URL}/api/profile/search?query=${query}`,
  },
};

export default endpoints;
