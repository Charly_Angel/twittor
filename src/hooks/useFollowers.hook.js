import { useState, useEffect } from "react";
import { isEmpty } from "lodash";
import { getFollowers } from "api/user.api";

export const useFollowers = ({ id }) => {
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [users, setUsers] = useState([]);
  const [noMoreUsers, setNoMoreUsers] = useState(false);

  useEffect(() => {
    setLoading(true);
    getFollowers({ id, page })
      .then(({ data }) => {
        isEmpty(data)
          ? setNoMoreUsers(true)
          : setUsers((items) => [...items, ...data]);

        setLoading(false);
      })
      .catch(() => {
        setUsers([]);
        setLoading(false);
      });
  }, [id, page]);

  const nextPage = () => {
    setPage(page + 1);
  };

  return [users, loading, noMoreUsers, nextPage];
};
