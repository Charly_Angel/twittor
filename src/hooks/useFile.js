import { useState, useCallback } from 'react';
import { useDropzone } from 'react-dropzone';

export const useFile = (url, configuration) => {
  const [file, setFile] = useState(null);
  const [fileUrl, setFileUrl] = useState(url);
  const onDropFile = useCallback((acceptedFile) => {
    const newFile = acceptedFile[0];
    setFileUrl(URL.createObjectURL(newFile));
    setFile(newFile);
  }, []);

  const { getRootProps, getInputProps } = useDropzone({
    ...configuration,
    onDrop: onDropFile,
  });

  return [file, fileUrl, getRootProps, getInputProps];
};
