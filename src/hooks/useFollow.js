import { useState, useEffect } from "react";
import { checkFollow, follow, unFollow } from "api/follow.api";

export const useFollow = (profileId) => {
  const [following, setFollowing] = useState(false);

  useEffect(() => {
    if (profileId) {
      checkFollow({ id: profileId })
        .then(({ error, data }) => {
          if (!error) setFollowing(data);
        })
        .catch(() => setFollowing(null));
    }
  }, [profileId]);

  const up = () => {
    follow({ id: profileId })
      .then(({ error }) => {
        if (!error) setFollowing(true);
      })
      .catch(() => setFollowing(false));
  };

  const down = () => {
    unFollow({ id: profileId })
      .then(({ error }) => {
        if (!error) setFollowing(false);
      })
      .catch(() => setFollowing(false));
  };

  const switchFollow = () => (following ? down() : up());

  return [following, switchFollow];
};
