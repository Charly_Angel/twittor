import { useState, useEffect } from "react";
import { isEmpty } from "lodash";
import { searchProfile } from "api/user.api";

export const useExplore = ({ query }) => {
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState([]);
  const [noMoreItems, setNoMoreItems] = useState(false);

  useEffect(() => {
    if (query) {
      setLoading(true);
      searchProfile({ query })
        .then(({ data }) => {
          isEmpty(data) ? setNoMoreItems(true) : setItems([...data]);

          setLoading(false);
        })
        .catch(() => {
          setItems([]);
          setLoading(false);
        });
    } else {
      setItems([]);
      setLoading(false);
    }
  }, [query]);

  return [items, loading, noMoreItems];
};
