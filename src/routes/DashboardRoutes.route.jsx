import React from "react";
import { Routes, Route } from "react-router-dom";
import Home from "pages/Home";
import User from "pages/User";
import Error404 from "pages/Errors/Error404";
import Users from "pages/Users";
import Explore from "pages/Explore";
import BasicLayout from "Layout/Basic";

export default function DashboardRoutes() {
  return (
    <BasicLayout>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/:id" element={<User />} />
        <Route path="/users" element={<Users />} />
        <Route path="/explore" element={<Explore />} />
        <Route path="*" element={<Error404 />} />
      </Routes>
    </BasicLayout>
  );
}
