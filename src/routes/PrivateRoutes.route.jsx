import React from 'react';
import { Navigate } from 'react-router-dom';

export default function PrivateRoutes({ children, isAuthenticated }) {
  return isAuthenticated ? children : <Navigate to='/login' />;
}
