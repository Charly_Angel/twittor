/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import Feedback from 'react-bootstrap/Feedback';
import FormControl from 'react-bootstrap/FormControl';
import { useField, ErrorMessage } from 'formik';

export function InputText(props) {
  const [field, meta] = useField(props);
  const { name } = props;
  return (
    <>
      <FormControl
        {...field}
        {...props}
        isInvalid={meta.touched && meta.error}
      />
      <ErrorMessage
        name={name}
        render={(message) => <Feedback type="invalid">{message}</Feedback>}
      />
    </>
  );
}
