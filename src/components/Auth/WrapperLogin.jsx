import React, { useState } from "react";
import { Button } from "react-bootstrap";
import BasicModal from "components/Modals/Basic";
import SignupForm from "components/Auth/SignUpForm";
import SignInForm from "components/Auth/SignInForm";
import LogoWhite from "assets/png/logo-white.png";

function WrapperLogin() {
  const [showModal, setShowModal] = useState(false);
  const [contentModal, setContentModal] = useState(null);

  const handleRegister = () => {
    setContentModal(<SignupForm setShowModal={setShowModal} />);
    setShowModal(true);
  };
  const handleLogin = () => {
    setContentModal(<SignInForm setShowModal={setShowModal} />);
    setShowModal(true);
  };

  const renderModal = () => (
    <BasicModal setShow={setShowModal} show={showModal}>
      {contentModal}
    </BasicModal>
  );

  return (
    <div>
      <img src={LogoWhite} alt="twittor" />
      <h2>Mira lo que está pasando en el mundo en este momento</h2>
      <h3>Únete a Twittor hoy mismo</h3>
      <Button variant="primary" onClick={handleRegister}>
        Regístrate
      </Button>
      <Button variant="outline-primary" onClick={handleLogin}>
        Inicia sesión
      </Button>
      {showModal && renderModal()}
    </div>
  );
}

export default WrapperLogin;
