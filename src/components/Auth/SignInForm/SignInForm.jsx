import { FormGroup } from "react-bootstrap";
import { toast } from "react-toastify";
import Button from "components/UI/Button";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { InputText } from "components/Form";
import { signIn } from "api/auth.api";
import { useAuth } from "hooks/useAuth.hook";
import "./styles.scss";

function initialValues() {
  return {
    email: "",
    password: "",
  };
}

function validationSchema() {
  return Yup.object({
    email: Yup.string()
      .email("Invalid email address")
      .required("email is required"),
    password: Yup.string()
      .min(8, "the password must be min 8 characters")
      .required("password is required"),
  });
}

export default function SignInForm({ setShowModal }) {
  const { login } = useAuth();

  return (
    <div className="sign-in-form">
      <h2>Iniciar sesión</h2>
      <Formik
        initialValues={initialValues()}
        validationSchema={validationSchema()}
        onSubmit={async (values) => {
          const { email, password } = values;
          const { data, error, message } = await signIn({ email, password });
          if (error) {
            toast.warning(message);
          } else {
            const { token } = data;
            if (token) {
              login(token);
              setShowModal(false);
            } else {
              toast.warning("No se pudo iniciar sesión");
            }
          }
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <FormGroup>
              <InputText
                type="email"
                name="email"
                placeholder="correo electrónico"
              />
            </FormGroup>

            <FormGroup>
              <InputText
                type="password"
                name="password"
                placeholder="contraseña"
              />
            </FormGroup>

            <Button variant="primary" type="submit" loading={isSubmitting}>
              Iniciar sesión
            </Button>
          </Form>
        )}
      </Formik>
    </div>
  );
}
