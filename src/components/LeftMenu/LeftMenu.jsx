import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faUser,
  faUsers,
  faPowerOff,
  faHashtag,
} from "@fortawesome/free-solid-svg-icons";
import TweetModal from "components/Modals/Tweet";
import { useAuth } from "hooks/useAuth.hook";
import LogoWhite from "assets/png/logo-white.png";
import "./styles.scss";

export default function LeftMenu() {
  const [showModal, setShowModal] = useState(false);
  const { user, logout } = useAuth();

  const renderModal = () => (
    <TweetModal show={showModal} setShow={setShowModal} />
  );

  return (
    <div className="left-menu">
      <img className="logo" src={LogoWhite} alt="twittor" />
      <Link to="/">
        <FontAwesomeIcon icon={faHome} /> Inicio
      </Link>
      <Link to="/explore">
        <FontAwesomeIcon icon={faHashtag} /> Explorar
      </Link>
      <Link to="/users">
        <FontAwesomeIcon icon={faUsers} /> Usuarios
      </Link>
      <Link to={`/${user?.sub}`}>
        <FontAwesomeIcon icon={faUser} /> Perfil
      </Link>
      <Link to="/" onClick={logout}>
        <FontAwesomeIcon icon={faPowerOff} /> Cerrar sesión
      </Link>

      <Button onClick={() => setShowModal(true)}>Twittoar</Button>

      {showModal && renderModal()}
    </div>
  );
}
