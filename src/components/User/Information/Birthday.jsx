import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import moment from 'moment';
import localization from 'moment/locale/es-mx';
import { faCalendar } from '@fortawesome/free-solid-svg-icons';

export function Birthday({ birthday }) {
  return (
    <p>
      <FontAwesomeIcon icon={faCalendar} />
      {moment(birthday).locale('es-mx', localization).format('LL')}
    </p>
  );
}
