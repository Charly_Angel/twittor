import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';

export function Location({ location }) {
  return (
    <p>
      <FontAwesomeIcon icon={faMapMarkerAlt} />
      {' '}
      {location}
    </p>
  );
}
