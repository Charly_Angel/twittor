import React from 'react';
import { Birthday } from './Birthday';
import { Location } from './Location';
import { WebSite } from './WebSite';

export function MoreInfo({ children }) {
  return <div className="more-info">{children}</div>;
}

MoreInfo.Location = Location;
MoreInfo.Birthday = Birthday;
MoreInfo.WebSite = WebSite;
