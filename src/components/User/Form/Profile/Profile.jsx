/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import { faCamera } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { FormGroup } from "react-bootstrap";
import { Formik, Form } from "formik";
import { toast } from "react-toastify";
import * as Yup from "yup";

import { InputText, InputDate } from "components/Form";
import { useFile } from "hooks/useFile";
import {
  uploadBannerApi,
  uploadAvatarApi,
  updateProfileApi,
} from "api/user.api";
import Button from "components/UI/Button";
import "./styles.scss";

function initialValues(user = {}) {
  return {
    name: user.name || "",
    biography: user.biography || "",
    location: user.location || "",
    birthday: user.birthday || new Date(),
    website: user.website || "",
  };
}

function validationSchema() {
  return Yup.object({
    name: Yup.string().required("name is required"),
    biography: Yup.string().required("biography is required"),
    location: Yup.string().required("location is required"),
    birthday: Yup.date().required("birthday is required"),
    website: Yup.string().url().required("website is required"),
  });
}

function configDropzone() {
  return {
    accept: "image/jpeg, image/png, image/jpg",
    noKeyboard: true,
    multiple: false,
  };
}

export default function Profile({ user, setShowModal, reloadData }) {
  const [bannerFile, bannerUrl, getRootBannerProps, getInputBannerProps] =
    useFile(user?.banner || null, configDropzone());

  const [avatarFile, avatarUrl, getRootAvatarProps, getInputAvatarProps] =
    useFile(user?.avatar || null, configDropzone());

  return (
    <div className="profile-form">
      <div
        className="banner"
        style={{ backgroundImage: `url('${bannerUrl}')` }}
        {...getRootBannerProps()}
      >
        <input {...getInputBannerProps()} />
        <FontAwesomeIcon icon={faCamera} />
      </div>
      <div
        className="avatar"
        style={{ backgroundImage: `url('${avatarUrl}')` }}
        {...getRootAvatarProps()}
      >
        <input {...getInputAvatarProps()} />
        <FontAwesomeIcon icon={faCamera} />
      </div>

      <Formik
        initialValues={initialValues(user)}
        validationSchema={validationSchema()}
        onSubmit={async (values) => {
          try {
            if (bannerFile) {
              await uploadBannerApi(bannerFile).catch(() =>
                toast.error("error al subir el banner"),
              );
            }

            if (avatarFile) {
              await uploadAvatarApi(avatarFile).catch(() =>
                toast.error("error al subir el banner"),
              );
            }
            await updateProfileApi(values);
            toast.success("Datos actualizados");
            reloadData(user.id);
            setShowModal(false);
          } catch (error) {
            toast.error("Error al actualizar los datos");
          }
        }}
      >
        {({ isSubmitting, values, handleChange }) => (
          <Form>
            <FormGroup>
              <InputText type="text" name="name" placeholder="Nombre" />
            </FormGroup>

            <FormGroup>
              <InputText
                as="textarea"
                row="3"
                name="biography"
                placeholder="Biografía"
              />
            </FormGroup>

            <FormGroup>
              <InputText type="text" name="website" placeholder="Sitio web" />
            </FormGroup>

            <FormGroup>
              <InputText type="text" name="location" placeholder="Ubicación" />
            </FormGroup>

            <InputDate
              name="birthday"
              placeholder="Fecha de nacimiento"
              selected={new Date(values.birthday)}
              onChange={(data) =>
                handleChange({ target: { name: "birthday", value: data } })
              }
            />

            <Button
              type="submit"
              className="btn-submit"
              variant="primary"
              loading={isSubmitting}
            >
              Actualizar
            </Button>
          </Form>
        )}
      </Formik>
    </div>
  );
}
