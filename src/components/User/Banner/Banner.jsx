import React, { useState } from "react";
import { Button } from "react-bootstrap";
import AvatarNotFound from "assets/png/avatar-no-found.png";
import { useAuth } from "hooks/useAuth.hook";
import { useFollow } from "hooks/useFollow";
import ConfigModal from "components/Modals/Config";
import ProfileForm from "components/User/Form/Profile";
import "./styles.scss";

export function Banner({ user, reloadData }) {
  const [showModal, setShowModal] = useState(false);
  const { user: userLogged } = useAuth();
  const [following, switchFollow] = useFollow(user?.id);

  const renderModal = () => (
    <ConfigModal show={showModal} setShow={setShowModal} title="Editar perfil">
      <ProfileForm
        user={user}
        setShowModal={setShowModal}
        reloadData={reloadData}
      />
    </ConfigModal>
  );

  return (
    <div
      className="banner"
      style={{ backgroundImage: `url('${user?.banner}')` }}
    >
      <div
        className="avatar"
        style={{
          backgroundImage: `url('${user?.avatar || AvatarNotFound}')`,
        }}
      />

      {user && (
        <div className="options">
          {userLogged.sub === user.id ? (
            <Button onClick={() => setShowModal(true)}>Editar perfil</Button>
          ) : (
            following !== null && (
              <Button
                onClick={switchFollow}
                className={`${following && "unfollow"}`}
              >
                <span>{following ? "Siguiendo" : "Seguir"}</span>
              </Button>
            )
          )}
        </div>
      )}

      {showModal && renderModal()}
    </div>
  );
}
