import { TOKEN } from "utils/constants";
import jwtDecode from "jwt-decode";

export function setToken(token) {
  localStorage.setItem(TOKEN, token);
}

export function getToken() {
  return localStorage.getItem(TOKEN) || null;
}

export function decodeToken(token) {
  return jwtDecode(token);
}

export function hasExpiredToken() {
  const token = getToken();
  if (!token) {
    return true;
  }
  const payload = jwtDecode(token);
  return payload.exp < Date.now() / 1000;
}

export function removeToken() {
  localStorage.removeItem(TOKEN);
}
