import React from "react";
import { Button, Spinner } from "react-bootstrap";
import { useTweets } from "hooks/useTweets";
import endpoints from "api/endpoints.api";
import ListTweets from "components/Tweets";
import "./styles.scss";

export default function Home() {
  const [tweets, moreTweets, loading, noMoreTweets] = useTweets({
    endpoint: endpoints.tweets.home,
  });
  return (
    <div className="home">
      <div className="home__title">
        <h2>Inicio</h2>
      </div>
      <div className="home__tweets">
        {tweets && <ListTweets tweets={tweets} />}
        {!noMoreTweets && (
          <Button onClick={moreTweets}>
            {loading ? (
              <Spinner
                as="span"
                animation="grow"
                size="sm"
                role="status"
                arian-hidden="true"
              />
            ) : (
              "Obtener más tweets"
            )}
          </Button>
        )}
      </div>
    </div>
  );
}
