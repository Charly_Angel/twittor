import React, { useEffect, useState } from "react";
import { Button, Spinner } from "react-bootstrap";
import { toast } from "react-toastify";
import { useParams } from "react-router-dom";
import { Banner } from "components/User/Banner";
import { Information } from "components/User/Information";
import Tweets from "components/Tweets";
import { useTweets } from "hooks/useTweets";
import endpoints from "api/endpoints.api";
import "./styles.scss";
import { getProfile } from "api/user.api";

export default function User() {
  const [user, setUser] = useState(null);
  const { id } = useParams();
  const [tweets, moreTweets, loading, noMoreTweets] = useTweets({
    endpoint: endpoints.tweets.user({ id }),
  });

  const getDataProfile = (profileId) => {
    getProfile({ id: profileId })
      .then((result) => {
        if (result.error) {
          toast.error(result.message);
        } else {
          setUser(result.data);
        }
      })
      .catch(() => toast.error("El usuario que has visitado no existe"));
  };

  useEffect(() => {
    getDataProfile(id);
  }, [id]);

  return (
    <div className="user">
      <div className="user__title">
        <h2>{user ? `${user.name}` : "Este usuario no existe"}</h2>
      </div>

      <Banner user={user} reloadData={getDataProfile} />
      {user && <Information user={user} />}
      <div className="user__tweets">
        <h3>Tweets</h3>
        {tweets && <Tweets tweets={tweets} />}

        {!noMoreTweets && (
          <Button onClick={moreTweets}>
            {loading ? (
              <Spinner
                as="span"
                animation="grow"
                size="sm"
                role="status"
                arian-hidden="true"
              />
            ) : (
              "Obtener más tweets"
            )}
          </Button>
        )}
      </div>
    </div>
  );
}
