import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import ContentImage from "components/Auth/ContentImage";
import WrapperLogin from "components/Auth/WrapperLogin";

import "./Auth.scss";

export function Auth() {
  return (
    <Container className="auth" fluid>
      <Row>
        <Col className="auth__left" xs={6}>
          <ContentImage />
        </Col>
        <Col className="auth__right" xs={6}>
          <WrapperLogin />
        </Col>
      </Row>
    </Container>
  );
}
