import { createContext } from "react";
import { useAuthProvider } from "./useAuthProvider.hook";

export const AuthContext = createContext({
  user: null,
  isAuthenticated: false,
  // eslint-disable-next-line no-unused-vars
  login: ({ email, password }) => {},
  logout: () => {},
});

export function AuthProvider({ children }) {
  const auth = useAuthProvider();
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
}
