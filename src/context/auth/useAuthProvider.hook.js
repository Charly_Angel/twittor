import { useState, useEffect } from "react";
import axios from "axios";
import {
  getToken,
  decodeToken,
  setToken,
  removeToken,
  hasExpiredToken,
} from "utils/handler-token.utility";

export function useAuthProvider() {
  const [user, setUser] = useState(null);
  const isAuthenticated = !!user;

  useEffect(() => {
    const token = getToken();

    if (token) {
      const payload = decodeToken(token);
      if (payload && !hasExpiredToken()) {
        axios.defaults.headers.common.Authorization = `Bearer ${token}`;
        setUser(payload);
      } else {
        delete axios.defaults.headers.common.Authorization;
        removeToken();
      }
    }
  }, []);

  const logout = () => {
    removeToken();
    delete axios.defaults.headers.common.Authorization;
    setUser(null);
  };
  const login = (token) => {
    setToken(token);
    axios.defaults.headers.common.Authorization = `Bearer ${token}`;
    const payload = decodeToken(token);
    setUser(payload);
  };

  return { user, isAuthenticated, login, logout };
}
