/* eslint-disable import/no-extraneous-dependencies */
import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import { resolve } from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      api: resolve("src/api/"),
      assets: resolve("src/assets/"),
      components: resolve("src/components/"),
      context: resolve("src/context/"),
      helpers: resolve("src/helpers/"),
      hooks: resolve("src/hooks/"),
      Layout: resolve("src/Layout/"),
      pages: resolve("src/pages/"),
      routes: resolve("src/routes/"),
      scss: resolve("src/scss/"),
      utils: resolve("src/utils/"),
    },
  },
});
